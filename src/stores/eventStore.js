import { observable, action } from 'mobx';
// import agent from '../agent';
import calendarStore from './calendarStore';

class EventStore {
    
  @observable events = [];
  
  @observable lists = [];
  
  @observable paths = [];
  
  @observable ids = {
    lists: [],
    events: [],
    paths: []
  };
  
  @action changeIds = (action, object) => {
    let ids = this.ids[object.type];
    switch (action) {
      case 'add':
        ids.push(object.id);
      break;
      case 'delete':
        ids = ids.filter(tempId => object.id !== tempId)
      break;
    }
  }
  
  @observable layers = [
      'money',
      'art',
      'success',
      'job',
      'stickerDay',
      'robo',
      'blog',
      'household',
      'up',
      'health',
      'post'
  ]
  
  @observable stickers = [
    'default.svg',
    'firstLeaves.svg',
    'newPet.svg',
    'seedToGround.svg',
    'water.svg',
    'pet.svg',
    'pills.svg',
    'house.svg',
    'parcel.svg',
    'wedding.svg',
    'airplane.svg',
    'swift.svg',
    'rgb.svg',
    'shopping-cart.svg',
    'portfolio.svg',
    'job.svg',
    'money.svg',
    'moneyplus.svg',
    'moneyminus.svg',
    'artichoke.svg',
    'snowflake.svg',
    'anonymous.svg',
    'close.svg',
    'treadmill.svg',
    'placeholder.svg',
    'lotus.svg',
    'gym.svg'
  ]
  
  @action addList = list => { // if list - copy
      let lst = list ? { ...list } : {
          title: "",
          children: [],
          type: 'lists',
          parents: []
      };
      lst.id = this.generateId('lists');
      this.changeIds('add', lst);
      this.lists.push(lst);
      this.saveToLS();
      return lst
  }
  
  @action addPath = path => { // if path - copy
      let pth = path ? { ...path } : {
          title: "",
          children: [],
          type: 'paths',
          parents: []
      };
      pth.id = this.generateId('paths');
      this.changeIds('add', pth);
      this.paths.push(pth);
      this.saveToLS();
      return pth
  }
  
  generateId = (type) => { 
    let randomId = Math.ceil(Math.random()*1000000);
    return this.ids[type].includes(randomId) ? this.generateId(type) : randomId
  }
  
  @action updateParent = (e, object, propName) => { // obj = path | list
      let obj = this.getObject(object);
      obj[propName] = e.target.value;
      this.saveToLS();
  }
  
  @action askDeleteParent = object => {
      calendarStore.dialog = {
          open: true,
          title: `Delete ${object.type.slice(0,-1)}?`,
          text: `You are going to delete "${object.title}"`,
          obj: object,
          action: 'delete'
      }
  }
  
  @action askDeleteEvent = event => {
      calendarStore.dialog = {
          open: true,
          title: "Delete event?",
          text: `You are going to delete "${event.title}"`,
          obj: event,
          action: 'delete'
      }
  }
  
  @action deleteParent = object => {
      console.log('delete', object.type, object.id);
      object.children.map(child => {
          this.unbindEventFrom(object, child) ;
      });
      this[object.type] = this.getArrayWithout(object);
      this.changeIds('delete', object);
  }
  
  test = () => {
      console.log('11111111');
  }
  
  @action unbindEventFrom = (parent, event) => {
      parent = this.getObject(parent);
      event = this.getObject(event);
      
      if (event && event.parents) {
         event.parents = event.parents.filter(pnt => pnt.id !== parent.id || pnt.type !== parent.type);
      }
      
      if (parent && parent.children) {
          parent.children = parent.children.filter(child => child.id !== event.id || child.type !== event.type);
      }
      
      this.saveToLS();
  }
  
  @action addEvent = event => { // if event - copy
      let evt = event ? { ...event } : {
          title: "",
          description: "",
          date: null,
          img: null,
          done: false,
          parents: [],
          type: 'events',
          layers: []
      };
      evt.id = this.generateId('events');
      
      this.changeIds('add', evt);
      this.events.push(evt);
      calendarStore.selectEvent(evt);
      this.saveToLS();
  }
  
  @action updateEvent = (e, selectedEvent, propName) => {
      const event = this.getObject(selectedEvent);
      const val = propName === 'priority' ? e : e.target.value;
      event[propName] = val;
      this.saveToLS();
  }
  
  @action setIcon = (selectedEvent,icon) => {
      let event = this.events.filter(event => event.id === selectedEvent.id)[0];
      event.img = icon;
      this.saveToLS();
  }
  
  @action deleteEvent = event => {
      let evt = event || calendarStore.selectedEvent;
      
      if (evt && evt.id) {
          
          evt.parents.map(parent => {
            this.unbindEventFrom(parent, evt);
          });
          
          this.changeIds('delete', evt);

          this.events = this.getArrayWithout(evt);
      }
  }
  
  @action setToday = () => {
      let event = calendarStore.selectedEvent;
      if (event) {
          this.getObject(event).date = new Date();
      }
      this.saveToLS();
  }
  
  @action toggleDone = (e, selectedEvent) => {
      let event = calendarStore.selectedEvent;
      let newStatus = e.target.checked;
      if (event) {
          let evt = this.getObject(event);
          evt.done = newStatus;
          evt.priority = newStatus ? 0 : -1;
      }
      this.saveToLS();
  }
  
  @action updateEventDate = (e, event) => {
      if (event) {
          this.getObject(event).date = new Date(e.target.value);
      }
      this.saveToLS();
  }
  
  @action layerChange = (e) => {
      this.getObject(calendarStore.selectedEvent).layers = e.target.value;
      this.saveToLS();
  }
  
  // filter functions
  
  getArrayWithout = (object) => {
      return this[object.type].filter(obj => (obj.id !== object.id) || (obj.type !== object.type) ) 
  }
  
  getObject = (object) => {
      if (!object) {
          console.warn("________________Empty object______________");
          return null
      }
      return this[object.type].filter(obj => (obj.id === object.id) && (obj.type === object.type))[0];
  }
  
  // LS functions
  
  @action toggleShortView = (list) => {
      //if (list) {
          this.getObject(list).short = !list.short;
      //}
  }
  
  @action readFromLS = () => {
      console.log('readFromLS');
      
      let {events, lists, paths, filters} = localStorage;
      
      events = events ? JSON.parse(events) : [];
      events = events.map(event => {
          let d = event.date;
          d = d ? d.split(".") : [];
          d = (d.length === 3) ? new Date(d[0],d[1],d[2]) : null; 
          event.date = d;
          this.changeIds('add', event);
          return event
      });
      
      lists = lists ? JSON.parse(lists) : [];
      lists.forEach(list => {
          return this.changeIds('add', list);
      });
      
      paths = paths ? JSON.parse(paths) : [];
      paths.forEach(path => this.changeIds('add', path));
      
      filters = filters ? JSON.parse(filters) : {};
      
      this.events = events;
      this.lists = lists;
      this.paths = paths;

      calendarStore.toggleFilter(filters || {});
  }
  
  saveToLS = () => {
      const events = this.events
      const evRes = events.map(event => {
          let ev = {...event};
          let d = ev.date;
          ev.date = d ? ['getFullYear','getMonth','getDate'].map(method => d[method]()).join('.') : null;
          return ev
      });

      const lstRes = this.lists.map(list => ({...list}));
      const pthRes = this.paths.map(path => ({...path}));
      
      localStorage.events = JSON.stringify(evRes);
      localStorage.lists = JSON.stringify(lstRes);
      localStorage.paths = JSON.stringify(pthRes);
      localStorage.filters = JSON.stringify(calendarStore.filters);
      
      console.log('saveToLS');
  }
  
  // drag stuff
  
  dragTarget = null;
  
  //whereDragStart = null;
  
  unbindDragEnter = () => {
      this.dragTarget = 'unbind';
  }
  
  parentDragEnter = parent => {
      this.dragTarget = parent;
  }
  
  dragTrueLeave = () => {
      this.dragTarget = null;
      //this.dzTrueLeave();
  }
  
  @action selectParent = parent => {
      calendarStore.selectedParent = parent;
  }
  
  @action addChildTo = (target, child) => {
      target = this.getObject(target);
      child = this.getObject(child);
      
      if (target && child) {
          const condition = !target.children.map(cld => [cld.id === child.id, cld.type === child.type].every(l=>l)).some(l=>l);
          if (condition && target.children && child.parents) {
              target.children.push({
                  type: child.type,
                  id: child.id
              });
              child.parents.push({
                type: target.type,
                id: target.id
              });
              
              //this.getObject(target).children = target.children;
              //this.getObject(child).parents = child.parents;
              
              this.saveToLS();
          } else {
              console.warn('#########')
              // debugger
          }
      }
  }
  
  @action eventDragEnd = event => {
      
      let target = this.dragTarget;
      let selectedParent = calendarStore.selectedParent;
      
      if ( target === 'unbind' ) {
          if (selectedParent) {
              if (selectedParent.getDate) {
                  event.date = "";
                  this.saveToLS();
              } else {
                  this.unbindEventFrom(selectedParent, event); // may be siple event?
              }
          } else {
              this.askDeleteEvent(event);
          }
      } else if (target === 'addToNewList') {
          this.addChildTo(this.addList(), event);
      } else if (target === 'addToNewPath') {
          this.addChildTo(this.addPath(), event);
      } else if (target && target.getDate) {
          event.date = target;
          this.saveToLS();
      } else {
          if (!(target === null && event === null)) {
              console.log('addChildTo: ', target, event);
              this.addChildTo(target, event);
          }
      }
      
      calendarStore.nullProps(['selectedParent']);
      this.dragTarget = null;
  }
  
  @action parentDragEnd = parent => {
    let target = this.dragTarget;
    let selectedParent = calendarStore.selectedParent;
    if (target === "unbind") {
        this.askDeleteParent(selectedParent);
    } else {
        if (!(target === null && selectedParent === null)) {
            console.log('addChildTo: ', target, selectedParent);
            this.addChildTo(target, selectedParent);
        }
    }
    
    calendarStore.nullProps(['selectedParent']);
    this.dragTarget = null;
  }
}

export default new EventStore();
