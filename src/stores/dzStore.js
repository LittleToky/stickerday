import { observable, action } from 'mobx';
// import agent from '../agent';
import eventStore from './eventStore';

class DzStore {
    
    @observable selectedPath = null;
    
    @observable selectedDZ = null;
    
    @action test = () => {
        console.log(11111);
    }
    
    @action dragInDZ = () => {
        console.log('over dz');
    }
    
    @action dragInPath = () => {
        console.log('over path');
    }
    
    @action dragOutPath = () => {
        console.log('out of path');
    }
    
    
    
    // @observable dzThrottler = {
    //     timeout: null,
    //     mayChange: true,
    //     redLight: () => { 
    //         this.mayChange = false;
    //         this.timeout = this.timeout ? this.timeout : setTimeout(this.greenLight, 2500);
    //         console.log('set red light');
    //     },
    //     greenLight: () => {
    //         this.mayChange = true;
    //         this.timeout = null
    //         console.log('set green light');
    //     }
    // }
    
    // @action selectDZ = (anchor) => {
    //     let throt = this.dzThrottler;
    //     if (throt.mayChange) {
    //       console.log('dzThrottler green');
    //       //console.log(anchor);
    //       this.selectedDZ = anchor;
    //       throt.redLight();
    //     } else {
    //       console.log('declined');
    //     }
    // }
    
    // @action dzTrueLeave = () => {
    //     let throt = this.dzThrottler;
    //     if (throt.mayChange) {
    //         console.log('dzThrottler green');
    //         //console.log('leave drop zone');
    //         this.selectedDZ = null;
    //         throt.redLight();
    //     } else {
    //       console.log('declined');
    //     }
    // }
  
}

export default new DzStore();
