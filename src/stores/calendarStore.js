import { observable, action } from 'mobx';
// import agent from '../agent';
import eventStore from './eventStore';

class CalendarStore {
    
  @observable initTime = new Date();
  
  @observable selectedEvent = null;
  
  @observable selectedParent = null;
  
  @observable selectedDay = null;
  
  @observable showMonthNum = [1,2]; // [num shown month before, num shown month after] 
  
  @action nullProps = (props) => {
    console.log('nulled');
    this.selectedDay = null;
    this.selectedParent = null;
    // props.map(prop => this[prop] = null)
  }
  
  @observable filters = {
      filterDone: [true, true],
      filterLists: {}
  };

  @action toggleFilter = (filterName, filterData) => {
      if (typeof(filterName) === 'string') {
          this.filters[filterName] = filterData;
      } else if (typeof(filterName) === 'object') {
          if (JSON.stringify(filterName) !== '{}') {
              this.filters = filterName; // all filters indeed, not filter name
          }
      }
  }
  
  @action formFilterLists = (list) => {
      let filter = this.filters.filterLists;
      let id = list.id;
      let visible = !filter[id];
      filter[id] = visible;
      this.toggleFilter('filterLists', filter);
      list.short = !visible;
      eventStore.saveToLS();
  }
  
  @action addMonth = pointer => this.showMonthNum[pointer] += 1
  
  @action toggleFilterDone = (e,i) => { // upgrade whith toggleFilter
      let filterDone = this.filters.filterDone;
      filterDone[i] = e.target.checked;
      this.filters.filterDone = filterDone;
  }
  
  @action selectEvent = (event) => {
      this.selectedEvent = event;
      console.log('this.selectedEventId: ', event.id);
  }
  
  applyDoneFilter = (events) => {
      const filterDone = this.filters.filterDone;
      filterDone.forEach((need,i) => {
          if (!need) {
              events = events.filter(event => {
                  return event.done === !!i
              });
          }
      });
      return events
  }
  
  applyListsFilter = (events) => {
      const filterLists = this.filters.filterLists;
      events = events.filter(evt => {
          const parents = evt.parents.filter(prt => prt.type === "lists"); // parents of the same type as filter
          return parents.length === 0 ? (true) : (parents.map(parent => filterLists[parent.id]).some(elem => elem))
      });
      
      return events
  }
  
  @observable initTime = new Date();
  
  @observable dialog = {
      open: false,
      title: "",
      text: "",
      obj: null,
      type: null,
      action: null
  }
  
  @action closeDialog = () => {
      this.dialog = {
          open: false,
          title: "",
          text: "",
          obj: null,
          type: null,
          action: null
      }
  }
  
  @action confirmDialog = () => {
      let dialog = this.dialog;
      let obj = dialog.obj;
      let type = obj.type;
      
      if (dialog.action === 'delete') {
          if (type === "events") {
              eventStore.deleteEvent(obj);
          } else if (type === "lists" || type === "paths") {
              eventStore.deleteParent(obj);
          }
          eventStore.saveToLS();
      }
      this.closeDialog();
  }
}

export default new CalendarStore();
