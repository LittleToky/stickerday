import ReactDOM from 'react-dom';
import promiseFinally from 'promise.prototype.finally';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { configure } from 'mobx';
import { Provider } from 'mobx-react';

import App from './components/App/';

import calendarStore from './stores/calendarStore';
import eventStore from './stores/eventStore';
import dzStore from './stores/dzStore';


const stores = {
  calendarStore,
  eventStore,
  dzStore
};

// For easier debugging
window._____APP_STATE_____ = stores;

promiseFinally.shim();
configure({
    enforceActions: true
});

ReactDOM.render((
  <Provider {...stores}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
), document.getElementById('root'));
