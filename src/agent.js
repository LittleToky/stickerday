import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

// const API_ROOT = 'http://wsc.gfi.tedlab.ru/api/v1/';
// 
// const encode = encodeURIComponent;
// 
// const handleErrors = err => {
//   if (err && err.response && err.response.status === 401) {
//     authStore.logout();
//   }
//   return err;
// };
// 
// const responseBody = res => res.body;
// 
// // const tokenPlugin = req => {
// //   if (commonStore.token) {
// //     req.set('authorization', `Token ${commonStore.token}`);
// //   }
// // };
// 
// const requests = {
//   del: url =>
//     superagent
//       .del(`${API_ROOT}${url}`)
//       .end(handleErrors)
//       .then(responseBody),
//   get: url =>
//     superagent
//       .get(`${API_ROOT}${url}`)
//       .end(handleErrors)
//       .then(responseBody),
//   put: (url, body) =>
//     superagent
//       .put(`${API_ROOT}${url}`, body)
//       .end(handleErrors)
//       .then(responseBody),
//   post: (url, body) =>
//     superagent
//       .post(`${API_ROOT}${url}`, body)
//       .end(handleErrors)
//       .then(responseBody),
// };
// 
// const TaskListsApi = {
//     getAll: () => requests.get('task-lists'),
//     getOne: id => requests.get(`task-lists/${id}`),
//     getTasks: id => requests.get(`task-lists/${id}/tasks`),
//     update: list => requests.post('task-list',list)
// };
// 
// const GroupsApi = {
//     getAll: () => requests.get('groups'),
//     getOne: id => requests.get(`groups/${id}`),
//     update: oldGroup => requests.post('group', oldGroup)
// };
// 
// const TasksApi = {
//     updateTask: task => requests.post('task', task)
// };
// 
// const Auth = {
//     login: pin => requests.post('auth', pin)
// };
// 
// const SсheduleApi = {
//     morning: user => requests.post('user_schedule', user)
// }
// 
// const SсheduleTaskApi = {
//     updateTaskStatus: scheduleList => requests.post('schedule_task', scheduleList)
// }
// 
// const SubApi = {
//     create: newSub => requests.post('user', newSub),
//     prescribe: array => requests.post('user_list', {'lists':array}),
//     getSubs: () => requests.get('users'),
//     delete: (id) => requests.del(`user/${id}`),
// };
// 
// export default {
//   SubApi,
//   SсheduleTaskApi,
//   SсheduleApi,
//   Auth,
//   TaskListsApi,
//   GroupsApi,
//   TasksApi
// };
