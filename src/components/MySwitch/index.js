import React from 'react';

import './styles/style.css';

class MySwitch extends React.Component {
    render() {
        const {checked, onChange, label} = this.props;
        return (
            <div className="myswitch">
                <input type="checkbox" checked={checked} onChange={onChange}/> {label}
            </div>
        )
    }
}

export default MySwitch;