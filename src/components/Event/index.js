import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import './styles/style.css'; 

@inject('calendarStore', 'eventStore')
@withRouter
@observer

class Event extends React.Component {
    render() {
        const { calendarStore, eventStore, event, cback, list, selectedEvent, withPriority } = this.props;
        let eventClass = "event";
        eventClass += event.done ? " done" : "";
        eventClass += selectedEvent && selectedEvent.id === event.id ? " selected" : "";
        let priorityClass = 'event-priority';
        if (withPriority) { // вынести
            const priorities={
                '-1' : 'after',
                '0' : 'regular',
                '2' : 'high',
                '5' : 'critical',
            }
            priorityClass += ' ' + priorities[(event.priority || 0) + ''];
        } 

        const trueCback = typeof(cback) === 'function' ? cback : function() {return}
        
        return (
            <React.Fragment>
                <div className={eventClass}>
                    <img
                        draggable="true"
                        onDragStart = {() => calendarStore.selectEvent(event)}
                        onDragEnd = {() => eventStore.eventDragEnd(event)}
                        className="event-icon"
                        src={"../stickers/svg/" + (event.img || 'default.svg')}
                        onClick={() => trueCback(event, list)}
                        alt=""
                    />
                </div>
                {withPriority && !event.done && (<div className={priorityClass}></div>)}
            </React.Fragment>
        )
    }
}

export default Event;