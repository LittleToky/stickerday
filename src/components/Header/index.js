import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import './styles/style.css'

@inject('calendarStore', 'eventStore')
@withRouter
@observer

class Header extends React.Component {
    render() {
        let path = this.props.location.pathname;
        return (
            <div className="header">
                <Link to="/paths">
                  <div className={(path === "/paths" ? 'active' : '') + " header-link"}>Paths</div>
                </Link>
                <Link to="/lists">
                  <div className={(path === "/lists" ? 'active' : '') + " header-link"}>Lists</div>
                </Link>
                <Link to="/calendar">
                  <div className={(path === "/calendar" || path === "/" ? 'active' : '') + " header-link"} >Calendar</div>
                </Link>
            </div>
        )
    }
}

export default Header;