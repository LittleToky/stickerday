import React from 'react';
import { withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';

import './styles/style.css';

@withRouter
@observer

class PrioritySelector extends React.Component {
    render() {
        
        const { changePriority, event } = this.props;
        const eventPriority = event.priority || 0;
        
        const priorities=[
            {
                value: -1,
                class: 'after'
            }, {
                value: 0,
                class: 'regular'
            }, {
                value: 2,
                class: 'high'
            }, {
                value: 5,
                class: 'critical'
            }
        ]
        return (
            <div className="priority-selector">
                Priority: 
                {priorities.map(priority => (
                    <div
                        text={priority.class}
                        key={priority.value}
                        className={priority.class + (priority.value === eventPriority ? ' active' : '')}
                        onClick={(e) => changePriority(priority.value, event, 'priority')}>
                    </div>
                ))}
            </div>
        )
    }
}

export default PrioritySelector;