import React from 'react';
import { withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';

import './styles/style.css';

@withRouter
@observer

class Trash extends React.Component {
    render() {
        return (
          <i className="ion-ios-trash trashbutton" onDragEnter={this.props.onEnter}/>
        )
    }
}

export default Trash;