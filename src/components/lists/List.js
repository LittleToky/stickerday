import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import Event from '../Event/';

import './styles/style.css';

@inject('calendarStore', 'eventStore')
@withRouter
@observer

class List extends React.Component {
    
    state = {sortByPriority: true};
    
    togglePriorSorter = () => {
        console.log('sortOn: ',!this.state.sortByPriority);
        this.setState({sortByPriority: !this.state.sortByPriority});
    }
    
    render() {
        const { calendarStore, eventStore, list } = this.props;
        let events = list.children.map(listEvent => eventStore.getObject(listEvent)).reverse();

        if (this.state.sortByPriority) { // вынести
            events = events.map(evt => {
                let temp = {...evt};
                temp.priority = temp.done ? -1 : (evt.priority || 0);
                return temp
            }).sort((a,b)=> b.priority - a.priority);
        }
        
        let listClass = 'list' + (list.short ? ' short' : '');
        let visible = calendarStore.filters.filterLists[list.id];
        let eyeClass = 'ion-ios-eye' + (visible ? ' active' : '');
        let sortClass = 'ion-code-download';
        
        const critical = events.filter(evt => evt.priority === 5).length;
        const high = events.filter(evt => evt.priority === 2).length;
        const regular = events.filter(evt => evt.priority === 0).length;
        
        return (
            <div 
              className={listClass}
              onDragEnter={() => eventStore.parentDragEnter(list)} 
              onMouseDown={() => eventStore.selectParent(list)}
              draggable='true'
              onDragStart = {() => eventStore.selectParent(list)}
              onDragEnd = {() => eventStore.parentDragEnd(list)}
            >
                <input placeholder="title" type="text" value={list.title || ''} onChange={e => eventStore.updateParent(e, list, 'title')}/>
                <div className="list-options">
                    <div>
                        {critical}
                        <div className='priority-ind critical'></div>
                        {high}
                        <div className='priority-ind high'></div>
                        {regular}
                        <div className='priority-ind regular'></div>
                    </div>
                    <i className={sortClass} onClick={this.togglePriorSorter}/>
                    <i className={eyeClass} onClick={() => calendarStore.formFilterLists(list)}/>
                    <i className="ion-android-more-horizontal" onClick={() => eventStore.toggleShortView(list)}/>
                    <i className="ion-ios-browsers" onClick={() => eventStore.addList(list)}/>
                    <i className="ion-close" onClick={() => eventStore.askDeleteParent(list)}/>
                </div>
                <div className="list-events">
                    {events.map((event,i) => (
                        <div key={i}>
                            <Event
                                event={event}
                                cback={calendarStore.selectEvent}
                                selectedEvent={calendarStore.selectedEvent}
                                withPriority={true}
                            />
                            <div
                                className="list-events-props"
                                onClick={() => calendarStore.selectEvent(event)}
                            >
                            {event.title}
                            </div>
                            <i className="ion-close out" onClick={() => eventStore.unbindEventFrom(list, event)}/>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default List;