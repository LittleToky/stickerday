import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import Sidebar from '../Sidebar/';
import List from './List'
import Trash from '../Trash/'

import './styles/style.css';

@inject('eventStore')
@withRouter
@observer

class Lists extends React.Component {
    render() {
        const { eventStore } = this.props;
        
        return (
            <div className="lists">
                <div className="lists-lists">
                    <div className="lists-lists-drag" onDragEnter={eventStore.dragTrueLeave}></div>
                    {eventStore.lists.map((list,i) => (
                        <List key={i} list={list} />
                    ))}
                    <div className="lists-addlist"  onClick={() => eventStore.addList()} onDragEnter={() => eventStore.parentDragEnter('addToNewList')} >+</div>
                </div>
                <Sidebar/>
                <Trash onEnter={eventStore.unbindDragEnter}/>
            </div>
        )
    }
}

export default Lists;