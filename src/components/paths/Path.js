import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import Event from '../Event/';
import DropZone from './DropZone';

import './styles/style.css';

@inject('dzStore', 'eventStore')
@withRouter
@observer

class Path extends React.Component {
    render() {
        const { dzStore, eventStore, path } = this.props;
        let events = path.children.map(pathEvent => eventStore.getObject(pathEvent));
        let pathClass = "path" + (path.short ? " short" : '');
        
        // const selectedDZ = eventStore.selectedDZ;
        
        const pathStartAnchor = {obj: path, position: 'start'}
        //let pathStartDZActive = false;
        
        // if (selectedDZ) {
        //     if (pathStartAnchor.obj === selectedDZ.obj && pathStartAnchor.position === selectedDZ.position) {
        //         pathStartDZActive = true;
        //     }
        // }
        
        return (
            <div 
              className={pathClass}
              onMouseDown={() => eventStore.selectParent(path)}
              onDragEnter={dzStore.dragInPath}
            >
                <input placeholder="title" type="text" value={path.title || ''} onChange={e => eventStore.updateParent(e, path, 'title')}/>
                <div className="path-options">
                    <i className="ion-android-more-horizontal" onClick={() => eventStore.toggleShortView(path)}/>
                    <i className="ion-ios-browsers" onClick={() => eventStore.addPath(path)}/>
                    <i className="ion-close" onClick={() => eventStore.askDeleteParent(path)}/>
                </div>
                <DropZone onEnterDZ={dzStore.dragInDZ}/>
                <div className="path-events">
                    {events.map((event,i) => (
                        <React.Fragment key={`ev-area_${event.id}`}>
                            <div key={i}>
                                <Event event={event}/>
                                <div className="path-events-props">{event.title}</div>
                                <i className="ion-close out" onClick={() => eventStore.unbindEventFrom(path, event)}/>
                            </div>
                            <DropZone draghover={false} anchor={{obj: event, position: 'after'}} key={`dz-after_${event.id}`} onEnterDZ={dzStore.dragIn}/>
                        </React.Fragment>
                    ))}
                </div>
            </div>
        )
    }
}

export default Path;