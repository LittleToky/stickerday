import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import Sidebar from '../Sidebar/';
import Path from './Path'
import Trash from '../Trash/'
import DropZone from './DropZone';

import './styles/style.css';

@inject('eventStore', 'dzStore')
@withRouter
@observer

class Paths extends React.Component {
    render() {
        const { eventStore, dzStore } = this.props;
        
        return (
            <div className="paths">
                <div className="paths-paths">
                    <div className="paths-paths-drag" onDragEnter={dzStore.dragOutPath}></div>
                    {eventStore.paths.map((path,i) => (
                        <Path key={i} path={path} />
                    ))}
                    <DropZone
                        draghover={false}
                        anchor={{obj: null, position: 'newPath'}}
                        key={`dz-newPath`}
                        onEnterDZ={dzStore.dragIn}
                    />
                </div>
                <Sidebar/>
                <Trash onEnter={eventStore.unbindDragEnter}/>
            </div>
        )
    }
}

export default Paths;