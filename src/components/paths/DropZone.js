import React from 'react';
import { withRouter } from 'react-router-dom';

import './styles/style.css';

@withRouter

class DropZone extends React.Component {
    render() {
        const { onEnterDZ, anchor, draghover } = this.props;
        const dzWrapClass = 'dz-wrap' + (draghover ? ' draghover' : '');
        
        return (
          <div className={dzWrapClass}>
              <div className="dz" onDragEnter={e => onEnterDZ(e)}>+</div>
          </div>
        )
    }
}

export default DropZone;