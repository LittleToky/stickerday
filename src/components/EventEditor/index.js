import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import Event from '../Event/';
import PrioritySelector from '../PrioritySelector/';
import MySwitch from '../MySwitch/';
import DateHelpers from '../../helpers/DateHelpers';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';

import './styles/style.css';

@inject('calendarStore', 'eventStore')
@withRouter
@observer

class EventEditor extends React.Component {
    render() {
        const { calendarStore, eventStore } = this.props;
        const selectedEventCopy = calendarStore.selectedEvent;
        let selectedId = selectedEventCopy ? selectedEventCopy.id : false;

        let event = selectedId ? eventStore.events.filter(event => event.id === selectedId)[0] : null;

        return (
            <div className="eventeditor">
                {event ? (
                    <div className="eventeditor-wrap">
                        <div  className="eventeditor-options">
                            <i className="ion-close"
                                onClick={() => eventStore.askDeleteEvent(event)}
                            />
                            <i className="ion-ios-browsers"
                                onClick={() => eventStore.addEvent(event)}
                            />
                            <i className="ion-ios-play reversex" />
                            <i className="ion-android-calendar"
                                onClick={eventStore.setToday}
                            />
                            <i className="ion-ios-play" />
                            <i className="ion-skip-forward" />
                            <i className="ion-android-more-horizontal" />
                        </div>
                        <div className="eventeditor-tileeditor">
                            <Event event={event} cback={eventStore.selectEvent}/>
                            <div className="eventeditor-tileeditor-popup">
                                {eventStore.stickers.map((sticker,i) => (
                                    <img
                                        key={i}
                                        src={"../stickers/svg/"+sticker}
                                        onClick={() => eventStore.setIcon(event,sticker)}
                                        alt=""
                                    />
                                ))}
                            </div>
                        </div>
                        <div  className="eventeditor-mainform">
                            <input
                                placeholder="title"
                                type="text" value={event.title || ''}
                                onChange={e => eventStore.updateEvent(e,event,'title')}
                                autoFocus={true}
                                key={event.id}
                            />
                            <textarea
                                placeholder="description"
                                value={event.description || ''}
                                onChange={e => eventStore.updateEvent(e,event,'description')}
                            />
                            <input
                                type="date"
                                value={DateHelpers.getDateValue(event.date)}
                                onChange={e => eventStore.updateEventDate(e,event)}
                            />
                            <MySwitch
                                checked={event.done}
                                onChange={e => eventStore.toggleDone(e,event)}
                                label="Done"
                            />
                            {!event.done && (<PrioritySelector event={event} changePriority={eventStore.updateEvent}/>)}
                            {false && (<Select
                              className="multiple"
                              multiple
                              value={event.layers}
                              onChange={e => eventStore.layerChange(e)}
                              input={<Input id="select-multiple-chip" />}
                              renderValue={selected => (
                                <div >
                                  {selected.map(value => (
                                    <Chip key={value} label={eventStore.layers[value]}  />
                                  ))}
                                </div>
                              )}
                            >
                              {eventStore.layers.map((layer,i) => (
                                <MenuItem key={i} value={i}>
                                  {layer}
                                </MenuItem>
                              ))}
                            </Select>)}
                        </div>
                    </div>
                 ) : (
                    <div>Click event to select</div>
                )}
            </div>
        )
    }
}

export default EventEditor;