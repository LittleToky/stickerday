import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import Sidebar from '../Sidebar/';
import CalendarMonth from './CalendarMonth';
import DateHelpers from '../../helpers/DateHelpers';
import Trash from '../Trash/'

import './styles/style.css'; 

@inject('calendarStore', 'eventStore')
@withRouter
@observer

class Calendar extends React.Component {
    render() {
        const { calendarStore, eventStore } = this.props;
        let initTime = calendarStore.initTime;
        // let initYear = initTime.getFullYear();
        // let initMonth =initTime.getMonth();
        
        let datesForMonths = [initTime];
        
        let events = eventStore.events;
        events = calendarStore.applyDoneFilter(events); // shorten
        events = calendarStore.applyListsFilter(events); // shorten
        
        for (let i = 0; i < calendarStore.showMonthNum[1]; i++) {
          let tempDateForward = datesForMonths[datesForMonths.length - 1]; 
          let resultDateForward = DateHelpers.neighbourMonthDate(tempDateForward,1);
          datesForMonths.push(resultDateForward);
        }
        
        for (let i = 0; i < calendarStore.showMonthNum[0]; i++) { 
          let resultDateBackward = DateHelpers.neighbourMonthDate(datesForMonths[0],-1)
          datesForMonths.unshift(resultDateBackward);
        }
        
        // datesForMonths.push(DateHelpers.neighbourMonthDate(datesForMonths[0],1));

        return (
            <div className="calendar">
                <div className="calendar-months">
                  <div className="calendar-months-drag" onDragEnter={eventStore.dragTrueLeave}></div>
                  <i className="ion-android-more-horizontal more" onClick={() => calendarStore.addMonth(0)}/>
                  {datesForMonths.map((date,i) => (
                      <CalendarMonth key={`d${date.getFullYear()}m${date.getMonth()}`} date={date} events={events}/>
                  ))}
                  <i className="ion-android-more-horizontal more" onClick={() => calendarStore.addMonth(1)}/>
                </div>
                <Sidebar />
                <Trash onEnter={eventStore.unbindDragEnter}/>
            </div>
        )
    }
}

export default Calendar;