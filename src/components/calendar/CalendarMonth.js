import React from 'react';
import { withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';
//import { inject, observer } from 'mobx-react';
import CalendarCell from './CalendarCell';
import DateHelpers from '../../helpers/DateHelpers';

import './styles/style.css'; 

// @inject('calendarStore', 'eventStore')
@withRouter
@observer

class CalendarMonth extends React.Component {
    
    state = {visible: true};
    
    toggleVisible = () => {
      this.setState({visible: !this.state.visible});
    }
    
    render() {
        const { date, events } = this.props;
        // const { calendarStore, eventStore, date, events } = this.props;
        
        const firstViewDay = DateHelpers.closestWeekDay(DateHelpers.firstDayMonth(date), 1, -1);
        const lastViewDay = DateHelpers.closestWeekDay(DateHelpers.lastDayMonth(date), 0, 1);
        
        let days = [firstViewDay];
        
        while (!this.areSameDates(days[days.length - 1],lastViewDay)) {
            let prev = days[days.length - 1];
            let current = this.neighbourDay(prev, 1);
            days.push(current);
        }
        
        const visible = this.state.visible;
        const shortenIconClass = 'ion-android-more-horizontal' + (visible ? ' active' : '')
        
        return (
            <div className="calendarmonth">
                <div className="calendarmonth-title">{date.toString().split(' ')[1]} {date.getFullYear()} <i className={shortenIconClass} onClick={this.toggleVisible}/></div>
                <div className="calendarmonth-days">
                    {visible && days.map((day,i) => (
                           <CalendarCell key={i} day={day} currentMonth={date} events={events}/>
                    ))}
                </div>
            </div>
        )
    }
    
    firstDayMonth(date) {
        return new Date(date.getFullYear(), date.getMonth(), 1);
    }
    
    lastDayMonth(date) {
        return new Date(date.getFullYear(), date.getMonth() + 1, 0);
    }
    
    neighbourDay(date,direction) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate() + direction)
    }
    
    closestWeekDay(date, weekDay, direction) {
        let from = date.getDay();
        if (weekDay === from) {
            return date
        }
        let dif = weekDay - date.getDay();
        return new Date(date.getFullYear(), date.getMonth(), date.getDate() + ((direction < 0) == (dif > 0) ? 7*direction + dif : dif))
    }
    
    areSameDates(a,b) {
        let res = ['getFullYear','getMonth','getDate'].map(item => {
            if (a && a[item] && b && b[item]) {
                return a[item]() === b[item]()
            }
        }).every(item => item);
        return res
    }
}

export default CalendarMonth;