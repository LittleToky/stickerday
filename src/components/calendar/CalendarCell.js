import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import DateHelpers from '../../helpers/DateHelpers';
import Event from '../Event/';

import './styles/style.css'; 

@inject('calendarStore', 'eventStore')
@withRouter
@observer

class CalendarCell extends React.Component {
    render() {
        const { calendarStore, eventStore, day, currentMonth, events } = this.props;
        let initTime = calendarStore.initTime;
        const thisDayEvents = events.filter(event => DateHelpers.areSameDates(event.date, day));
        
        let monthCond = day.getMonth() !== currentMonth.getMonth();
        
        let dayClass = "calendarcell";
        dayClass += (day.getMonth() == initTime.getMonth() ? "" : " disabled");
        dayClass += (monthCond ? " transp" : "");
        dayClass += (DateHelpers.areSameDates(day, initTime) ? " today" : "");
        
        return (
            <div className={dayClass} onDragEnter={() => eventStore.parentDragEnter(day)}  onMouseDown={() => eventStore.selectParent(day)} >
                <div className="calendarcell-date">{day.toString().split(' ')[1]} {day.toString().split(' ')[2]}</div>
                {
                    thisDayEvents.length > 0 && !monthCond ? (
                        <div className="calendarcell-events">
                            {thisDayEvents.map((event,i) => (
                               <Event
                                   key={i}
                                   event={event}
                                   cback={calendarStore.selectEvent}
                                   selectedEvent={calendarStore.selectedEvent}
                               />
                            ))}
                        </div>) : ""
                }
            </div>
        )
    }
}

export default CalendarCell;