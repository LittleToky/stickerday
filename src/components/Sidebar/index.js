import React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import MySwitch from '../MySwitch/';
import Event from '../Event/';
import EventEditor from '../EventEditor/';

import './styles/style.css';

@inject('calendarStore', 'eventStore')
@withRouter
@observer

class Sidebar extends React.Component {
    render() {
        const { calendarStore, eventStore } = this.props;
        let events = eventStore.events;
        
        events = calendarStore.applyDoneFilter(events);
        events = calendarStore.applyListsFilter(events);
        
        return (
            <div className="sidebar">
                <div className="sidebar-title">Events</div>
                <div className="sidebar-events">
                    {events.map((event,i) => (
                       <Event
                           key={i}
                           event={event}
                           cback={calendarStore.selectEvent}
                           selectedEvent={calendarStore.selectedEvent}
                        />
                    ))}
                    <div className="sidebar-addevent" onClick={() => eventStore.addEvent()}>+</div>
                </div>
                <MySwitch label="Done" checked={calendarStore.filters.filterDone[0]} onChange={e => calendarStore.toggleFilterDone(e,0)}/>
                <MySwitch label="Undone" checked={calendarStore.filters.filterDone[1]} onChange={e => calendarStore.toggleFilterDone(e,1)}/>
                <br/>
                <EventEditor/>
            </div>
        )
    }
}

export default Sidebar;