import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import Header from '../Header/';
import Calendar from '../calendar/Calendar';
import Lists from '../lists/Lists';
import Paths from '../paths/Paths';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import './styles/style.css'; 

@withRouter

@inject('eventStore', 'calendarStore')
@observer

export default class App extends React.Component {
  componentDidMount() {
      this.props.eventStore.readFromLS();
  }

  render() {
      let calendarStore = this.props.calendarStore;
      let dialog = calendarStore.dialog;
      return (
        <div>
          <Header />
          <Switch>
            <Route exact path="/" component={Calendar} />
            <Route path="/calendar" component={Calendar} />
            <Route path="/lists" component={Lists} />
            <Route path="/paths" component={Paths} />
          </Switch>
          
            <Dialog
                open={dialog.open}
                onClose={calendarStore.closeDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{dialog.title}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">{dialog.text}</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={calendarStore.closeDialog} color="primary">
                        Disagree
                    </Button>
                    <Button onClick={calendarStore.confirmDialog} color="primary" autoFocus>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
          
        </div>
      );
  }
}
