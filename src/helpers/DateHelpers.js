export default class DateHelpers {
    static firstDayMonth = date => {
        return new Date(date.getFullYear(), date.getMonth(), 1);
    }
    
    static lastDayMonth = date => {
        return new Date(date.getFullYear(), date.getMonth() + 1, 0);
    }
    
    static neighbourDay = (date,direction) => {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate() + direction)
    }
    
    static neighbourMonthDate = (date, direction) => {
      let dh = DateHelpers;
      let monthEdgeDay = direction > 0 ? dh.lastDayMonth(date) : dh.firstDayMonth(date);
      return dh.neighbourDay(monthEdgeDay, direction)
    }
    
    static getDateValue = date => {
        return date ? [date.getFullYear()].concat(['getMonth','getDate'].map(method => {
            let val = date[method]();
            val += method === 'getMonth' ? 1 : 0;
            return (val < 10 ? "0" : "") +val.toString()
        })).join('-') : ''
    }
    
    static closestWeekDay = (date, weekDay, direction) => {
        let from = date.getDay();
        if (weekDay === from) {
            return date
        }
        let dif = weekDay - date.getDay();
        return new Date(date.getFullYear(), date.getMonth(), date.getDate() + ((direction < 0) == (dif > 0) ? 7*direction + dif : dif))
    }
    
    static areSameDates = (a,b) => {
        let res = ['getFullYear','getMonth','getDate'].map(function(item) {
            if (a && a[item] && b && b[item]) {
                return a[item]() === b[item]()
            }
        }).every((item)=>{return item});
        return res
    }
}